CKEDITOR.addStylesSet( 'mystyles',
[
     // Object Styles � a, embed, hr, img, li, object, ol, table, td, tr and ul
     { name : 'Image Left', element : 'div', attributes : { 'class' : 'imageryfnleft' } },     
     { name : 'Image Right', element : 'div', attributes : { 'class' : 'imageryfnright' } },          
     { name : 'Caption', element : 'p', attributes : { 'class' : 'caption' } },
     
     // Block Styles � address, div, h1, h2, h3, h4, h5, h6, p and pre
     { name : 'Heading 3', element : 'h3' },
     { name : 'Heading 4', element : 'h4' },


]);