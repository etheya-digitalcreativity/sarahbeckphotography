<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>
 
<?php echo $form_edit; ?>
	
 	<table class="mainTable padTable" border="0" cellspacing="0" cellpadding="0">
		<thead class="">
			<tr>
				<th colspan="2">
					<strong><?=lang($module_name.'_delete')?>  </strong><br />
					<?=lang($module_name.'_delete_description')?>
					<input  type='hidden' name='id'  value='<?=$view['id']?>' size='90' maxlength='100' />
 					
				</th>
			</tr>
		</thead>
		<tbody>
			<tr class="<?php echo alternator('even', 'odd');?>">
				<td>
					 <?=$view['label']?> (<?=$view['short_name']?>)
 				</td>
				<td style='width:50%;'>
 					<input   type='checkbox' name='delete'  value='yes' checked="checked" />  <?=lang('delete_when_checked') ?>
				</td>
 			</tr>
 		</tbody>
	</table>
	<p><input type="submit" name="submit" value="<?=lang('submit')?>" class="submit" /></p>
	
	</form>
 